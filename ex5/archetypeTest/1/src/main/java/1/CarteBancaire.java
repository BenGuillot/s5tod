package 1;

/**
 * 
 * @author Guillot Benjamin 
 *
 */
public class CarteBancaire {
	protected int solde;
	
	/**
	 *  Constructeurs par defaut 
	 *  met le solde à 0
	 */
	public CarteBancaire() {
		this.solde = 0;
	}
	
	/**
	 * Constructeur avec paramètre 
	 * @param psolde initialise le solde à psolde
	 */
	public CarteBancaire(int psolde) {
		this.solde = psolde;
	}
	
	/**
	 * Getter du solde
	 * @return le solde 
	 */
	public int getSolde () {
		return this.solde;
	}
	
	/**
	 * Setter solde 
	 * @param montant change la valeur du solde pour
	 * 			      qu'elle soit égale à montant
	 */
	public void setSolde (int montant) {
		this.solde = montant;
	}
	
	/**
	 * Methode d'implémentation du credit
	 * @param valeur montant à ajouter au solde 
	 * @return le solde apres un credit
	 * @throws ExceptionCredit
	 */
	public int credit (int valeur) throws ExceptionCredit{
		if(valeur > 0) {
			this.solde = this.solde + valeur;
			return valeur;
		}
		else throw new ExceptionCredit ();
	}
	
	/**
	 * Méthode d'implémentation du debit 
	 * @param valeur montant à soustraire au solde 
	 * @throws ExceptionDebit
	 */
	public void debit (int valeur) throws ExceptionDebit {
		if(valeur <= this.solde && valeur > 0) {
			this.solde = this.solde - valeur;
		}
		else throw new ExceptionDebit();
	}
	
	/**
	 * Methode d'implémentation de transfert d'argent entre 2 compte 
	 * @param destinataire destinataire du virement 
	 * @param valeur montant de la transaction
	 * @throws VirementException
	 */
	public void virement (CarteBancaire destinataire, int valeur) throws VirementException{
		if (valeur <= this.solde) {
			destinataire.solde= destinataire.solde + valeur;
			this.solde = this.solde - valeur;
		}
		else throw new VirementException();
	}
}
